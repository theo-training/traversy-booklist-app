// Book Class: Represtents a Book
class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}
// UI Class: Handle UI Tasks
class UI {
  static displayBooks() {
    const storedBooks = [
      {
        title: 'Total Recall: My Unbelievably True Life Story',
        author: 'Arnold Schwarzenegger',
        isbn: '382834065',
      },
      {
        title: 'The Open Society and Its Enemies',
        author: 'Karl Popper',
        isbn: '39428734',
      },
    ]
    const booksFromStorage = JSON.parse(localStorage.getItem('books'));
    const books = booksFromStorage && booksFromStorage.length > 0 ? booksFromStorage :  storedBooks

    books.forEach(book => UI.addBookToList(book));
  }
  
  static addBookToList(book) {
    const booksList = document.getElementById('book-list');
    const bookElem = `
     <tr>
      <th>${book.title}</th>
      <th>${book.author}</th>
      <th>${book.isbn}</th>
      <th><a href="#" class="btn btn-danger btn-sm delete">x</a></th>
     </tr>
    `
    booksList.insertAdjacentHTML('beforeend', bookElem);
    
  }

  static deleteBookFromList(book) {
    book.remove();
  }

  static showAlert(message, className) {
    const alertDiv = document.createElement('div');
    alertDiv.className = `alert alert-${className}`;
    alertDiv.innerHTML = message;
    document.body.appendChild(alertDiv);
    // const container = document.querySelector('.container');
    // const form = container.querySelector('#book-form');
    // container.insertBefore(alertDiv, form);
    setTimeout(()=>alertDiv.parentElement.removeChild(alertDiv),3000);
  }
}
// Store Class: Handles Storage
class Store {
  static getBooks() {
    return JSON.parse(localStorage.getItem('books')) || [];
  }
  static addBook(book) {
    const books = Store.getBooks();
    books.push(book);
    localStorage.setItem('books', JSON.stringify(books));
  }
  static removeBook(isbn) {
    const books = Store.getBooks().filter(book => book.isbn !== isbn);
    localStorage.setItem('books', JSON.stringify(books));
  }
}

// Event: Display Books
document.addEventListener('DOMContentLoaded', UI.displayBooks);

// Event: Add a Book
const bookForm = document.querySelector('#book-form');
bookForm.addEventListener('submit', e => {
  e.preventDefault();
  // Get form values
  const bookParams = ['title', 'author', 'isbn'].map(input => bookForm.elements[input].value);
  
  // Validate
  if (bookParams.some(input => input === '')) {
    UI.showAlert('Please fill in all the fields', 'danger');
  } else {
    // Instantiate book
    const book = new Book(...bookParams)
    
    // Add book to store
    Store.addBook(book);
    // Add book to UI
    UI.addBookToList(book);
    UI.showAlert('Book added successfully', 'success');

    // Clear fields 
    bookForm.reset();
  }
})

// Event: Remove a Book
document.getElementById('book-list').addEventListener('click', (e) => {
  if (e.target.classList.contains('delete')) {
    const deleteColumn = e.target.parentElement;
    const isbnToRemove = deleteColumn.previousElementSibling.innerText;
    const bookToRemove =  deleteColumn.parentElement;
    console.log('bookToRemove = ', bookToRemove);
    console.log('isbnToRemove = ', isbnToRemove);
    Store.removeBook(isbnToRemove);
    UI.deleteBookFromList(bookToRemove);
    UI.showAlert(`Book with #ISBN ${isbnToRemove} Removed`, 'warning');
  }
})